# 小程序网站项目
基于微信小程序的网站前端

# 功能
该项目以ShirneCMS为接口端实现原生小程序的企业官网展示，有banner图，产品展示，新闻，公司介绍，地址/地图位置展示<br />
已含简单的oauth授权管理，小程序授权登录<br />
代码层面实现了html代码解析，仅过滤小程序支持的标签和属性

# 后端项目
[ShirneCMS](https://gitee.com/shirnecn/ShirneCMS)

# 功能截图

![首页](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/1.png "首页")
![产品展示](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/2.png "产品展示")
![新闻](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/3.png "新闻")
![新闻详情](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/4.png "新闻详情")
![公司介绍](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/5.png "公司介绍")
![地图](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/6.png "地图")